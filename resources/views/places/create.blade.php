<form action="{{ route('places.store') }}" method="post">
    @csrf
    <div>
        <label for="name">Nama Tempat:</label>
        <input type="text" id="name" name="name" value="{{ old('name') }}" required>
    </div>
    <div>
        <label for="latitude">Latitude:</label>
        <input type="text" id="latitude" name="latitude" value="{{ old('latitude') }}" required>
    </div>
    <div>
        <label for="longitude">Longitude:</label>
        <input type="text" id="longitude" name="longitude" value="{{ old('longitude') }}" required>
    </div>
    <button type="submit">Tambah Tempat</button>
</form>
