

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $place->name }}</div>

                    <div class="card-body">
                        <p>Latitude: {{ $place->latitude }}</p>
                        <p>Longitude: {{ $place->longitude }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="map" style="width: 100%; height: 400px;"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/openlayers/9.1.0/dist/ol.min.js" integrity="sha512-pujHCzGy+uHgmpPTzRobiD7i4PptQ0vHtc7705RIcLkImItwTLQX3tFZtemb5yKBm99BceYm+m2+P9yUBjw15Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <script>
        const map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([{{ $place->longitude }}, {{ $place->latitude }}]),
                zoom: 15
            })
        });

        fetch('/geojson/xample1.geojson')
            .then(response => response.json())
            .then(data => {
                const format = new ol.format.GeoJSON();
                const feature = format.readFeature(data);
                const vectorSource = new ol.source.Vector({
                    features: [feature]
                });
                const vectorLayer = new ol.layer.Vector({
                    source: vectorSource
                });
                map.addLayer(vectorLayer);
                map.getView().fit(vectorSource.getExtent(), { padding: [50, 50, 50, 50] });
            });
    </script>
@endsection
