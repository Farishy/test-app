<form action="{{ route('places.update', $place->id) }}" method="post">
    @csrf
    @method('PUT')
    <div>
        <label for="name">Nama Tempat:</label>
        <input type="text" id="name" name="name" value="{{ $place->name }}" required>
    </div>
    <div>
        <label for="latitude">Latitude:</label>
        <input type="text" id="latitude" name="latitude" value="{{ $place->latitude }}" required>
    </div>
    <div>
        <label for="longitude">Longitude:</label>
        <input type="text" id="longitude" name="longitude" value="{{ $place->longitude }}" required>
    </div>
    <button type="submit">Simpan Perubahan</button>
</form>
