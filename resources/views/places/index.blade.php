<h1>Daftar Semua Tempat</h1>

<table>
    <thead>
        <tr>
            <th>Nama</th>
            <th>Latitude</th>
            <th>Longitude</th>
        </tr>
    </thead>
    <tbody>
       @foreach ($places as $place)
        <tr>
            <td>{{ $place->name }}</td>
            <td>{{ $place->latitude }}</td>
            <td>{{ $place->longitude }}</td>
            <td>
                <form action="{{ route('places.edit', $place->id) }}" method="get">
                    <button type="submit" >Edit</button>
                </form>
            </td>
            <td>
            <td>
                <form action="{{ route('places.destroy', $place->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('Apakah Anda yakin ingin menghapus tempat ini?')">Hapus</button>
                </form>
            </td>
            <td>
            <form>
              <a href="{{ route('places.show', ['id' => $place->id]) }}">Lihat Detail</a>
            </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>




