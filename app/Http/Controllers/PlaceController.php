<?php

namespace App\Http\Controllers;

use App\Models\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::all();
    return view('places.index', compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('places.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   {
    $validator = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        'latitude' => [
            'required',
            'numeric',
            'between:-90,90', 
        ],
        'longitude' => [
            'required',
            'numeric',
            'between:-180,180', 
        ],
    ]);


    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator)
            ->withInput()
            ->with('error', 'Ada kesalahan dalam input Anda.');
    }

   
    $place = new Place();
    $place->name = $request->input('name');
    $place->latitude = $request->input('latitude');
    $place->longitude = $request->input('longitude');
    $place->save();

    return redirect()->route('places.index')->with('success', 'Tempat berhasil ditambahkan.');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place = Place::findOrFail($id);
        return view('places.show', compact('place'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place = Place::findOrFail($id);
        return view('places.edit', compact('place'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'latitude' => [
                'required',
                'numeric',
                'between:-90,90',
            ],
            'longitude' => [
                'required',
                'numeric',
                'between:-180,180', 
            ],
        ]);
    
         if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('error', 'Ada kesalahan dalam input Anda.');
        }
    
       
        $place = Place::findOrFail($id);
        $place->name = $request->input('name');
        $place->latitude = $request->input('latitude');
        $place->longitude = $request->input('longitude');
        $place->save();
    
        return redirect()->route('places.index')->with('success', 'Data tempat berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Temukan tempat berdasarkan ID
    $place = Place::findOrFail($id);

    // Hapus tempat
    $place->delete();

    return redirect()->route('places.index')->with('success', 'Data tempat berhasil dihapus.');

    }
}
